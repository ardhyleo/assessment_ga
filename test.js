// create function to determine the given expression is correct or not
// expression will be string of open bracket nor close bracket e.g ({[[()]]})
// your function should determine the expression is it correct or not
// examples:
// ({[]}) => true
// ([][]{})=> true
// ({)(]){[} => false
// [)()] => false

function test(data) {
  const splitData = data.split("");
  for (let i = 0; i < splitData.length; i++) {
      if(splitData[i] == "(") {
          if(splitData[i + 1] == ")") {
              return true
            } else if(splitData[i] == "{") {
                    if(splitData[i + 1] == "}"){
                        return true
                    }
            }
      }
